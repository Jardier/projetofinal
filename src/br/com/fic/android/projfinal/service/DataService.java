package br.com.fic.android.projfinal.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.IntentService;
import android.content.Intent;
import android.os.Handler;
import android.widget.Toast;

public class DataService extends IntentService {

	Handler handler;
	TimerTask time;

	public DataService() {
		super("DataService");
		handler = new Handler();

		time = new TimerTask() {

			@Override
			public void run() {
				handler.post(new Runnable() {
					final String data = getDataHoraJson();

					@Override
					public void run() {
						Toast.makeText(getApplicationContext(), "Data: " + data, Toast.LENGTH_LONG).show();
					}
				});

			}
		};

	}

	@Override
	protected void onHandleIntent(Intent intent) {
		(new Timer()).schedule(time, 1000, 5000);
	}

	public String getDataHoraJson() {
		String data = "";
		String hora = "";
		HttpPost request = new HttpPost("http://date.jsontest.com/");
		request.setHeader("Accept", "application/json");
		request.setHeader("Content-type", "application/json");
		DefaultHttpClient httpClient = new DefaultHttpClient();
		HttpResponse response;
		try {
			response = httpClient.execute(request);

			BufferedReader r = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuilder sb = new StringBuilder();
			String responseAsString = null;
			while ((responseAsString = r.readLine()) != null)
				sb.append(responseAsString);
			responseAsString = sb.toString();
			JSONObject responseJSON = new JSONObject(responseAsString);
			data = responseJSON.getString("date");
			hora = responseJSON.getString("time");

		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return data + " " + hora;
	}

}
