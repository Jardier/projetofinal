package br.com.fic.android.projetofinal.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.NotificationManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import br.com.fic.android.projetofinal.R;
import br.com.fic.android.projetofinal.controller.ListContactFragment.OnContatoSelectedListener;
import br.com.fic.android.projfinal.service.DataService;

public class PrincipalActivity extends ActionBarActivity implements OnContatoSelectedListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.list_contact);

		if (findViewById(R.id.fragment_container) != null) {

			if (savedInstanceState != null) {
				return;
			}

			ListContactFragment listFrag = new ListContactFragment();

			listFrag.setArguments(getIntent().getExtras());

			getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, listFrag).commit();

			Runnable jsonRunnable = new Runnable() {
				public void run() {
					final String resultadoJSON = showIp();
					runOnUiThread(new Runnable() {

						public void run() {
							showNotification(resultadoJSON);
						}
					});
				}
			};
			Thread thread = new Thread(null, jsonRunnable, "jsonRunnable");
			thread.start();
			Intent intent = new Intent(PrincipalActivity.this, DataService.class);
			startService(intent);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.principal, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onContatoSelected(int position) {
		LineContactFragment lineFrag = (LineContactFragment) getSupportFragmentManager().findFragmentById(R.id.descricao_contato);

		if (lineFrag != null) {

			lineFrag.updateArticleView(position);

		} else {
			LineContactFragment newFrag = new LineContactFragment();
			Bundle args = new Bundle();
			args.putInt(LineContactFragment.ARG_POSITION, position);
			newFrag.setArguments(args);
			FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
			transaction.replace(R.id.fragment_container, newFrag);
			transaction.addToBackStack(null);

			transaction.commit();

		}

	}

	private void showNotification(String ip) {

		NotificationCompat.Builder mBuilder;
		mBuilder = new NotificationCompat.Builder(this).setSmallIcon(R.drawable.ic_launcher).setContentTitle("Notification").setContentText("IP:" + ip);
		NotificationManager mNotifyMgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		mNotifyMgr.notify(1, mBuilder.build());

	}

	public String showIp() {
		String ip = "";
		HttpPost request = new HttpPost("http://ip.jsontest.com/");
		request.setHeader("Accept", "application/json");
		request.setHeader("Content-type", "application/json");
		DefaultHttpClient httpClient = new DefaultHttpClient();
		HttpResponse response;
		try {
			response = httpClient.execute(request);

			BufferedReader r = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuilder sb = new StringBuilder();
			String responseAsString = null;
			while ((responseAsString = r.readLine()) != null)
				sb.append(responseAsString);
			responseAsString = sb.toString();
			JSONObject responseJSON = new JSONObject(responseAsString);
			ip = responseJSON.getString("ip");

		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return ip;
	}
}
