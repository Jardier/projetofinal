package br.com.fic.android.projetofinal.controller;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import br.com.fic.android.projetofinal.R;


public class LineContactFragment extends Fragment {

	final static String ARG_POSITION = "position";
	int mCurrentPosition = -1;
	private TextView article;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		if (savedInstanceState != null) {
			mCurrentPosition = savedInstanceState.getInt(ARG_POSITION);
		}

		return inflater.inflate(R.layout.line_contact, container, false);
	}

	@Override
	public void onStart() {
		super.onStart();
		Bundle args = getArguments();
		if (args != null) {
			updateArticleView(args.getInt(ARG_POSITION));
		} else if (mCurrentPosition != -1) {
			updateArticleView(mCurrentPosition);
		}
		article = (TextView) getActivity().findViewById(R.id.descricao_contato);
		article.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				arg0.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.config));

			}
		});
	}

	public void updateArticleView(int position) {
		article = (TextView) getActivity().findViewById(R.id.descricao_contato);

		article.setText(getTelefoneEmailContato(position));
		mCurrentPosition = position;
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt(ARG_POSITION, mCurrentPosition);
	}

	private String getTelefoneEmailContato(int position) {
		List<String> lista = new ArrayList<String>();
		Uri PhoneCONTENT_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
		String Phone_CONTACT_ID = ContactsContract.CommonDataKinds.Phone.CONTACT_ID;
		Uri EmailCONTENT_URI = ContactsContract.CommonDataKinds.Email.CONTENT_URI;
		String EmailCONTACT_ID = ContactsContract.CommonDataKinds.Email.CONTACT_ID;
		Uri CONTENT_URI = ContactsContract.Contacts.CONTENT_URI;
		String DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME;
		String _ID = ContactsContract.Contacts._ID;
		String phoneNumber = null;
		String DATA = ContactsContract.CommonDataKinds.Email.DATA;
		String NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER;
		String telefone = "";
		ContentResolver contentResolver = getActivity().getContentResolver();
		++position;
		Cursor cursor = contentResolver.query(CONTENT_URI, null, _ID + " = " + position, null, null);

		if (cursor.getCount() > 0) {

			while (cursor.moveToNext()) {
				String contact_id = cursor.getString(cursor.getColumnIndex(_ID));
				String email = "";

				Cursor phoneCursor = contentResolver.query(PhoneCONTENT_URI, null, Phone_CONTACT_ID + " = ?", new String[] { contact_id }, null);
				telefone += "Telefone:";
				while (phoneCursor.moveToNext()) {
					phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(NUMBER));
					telefone += "\n" + phoneNumber;
					lista.add(telefone);

				}
				phoneCursor.close();

				Cursor emailCursor = contentResolver.query(EmailCONTENT_URI, null, EmailCONTACT_ID + " = ?", new String[] { contact_id }, null);

				telefone += "\n\nEmail:" + email;
				while (emailCursor.moveToNext()) {
					email = emailCursor.getString(emailCursor.getColumnIndex(DATA));
					telefone += "\n" + email;
					lista.add(telefone);

				}
				emailCursor.close();

			}
		}
		return telefone;

	}
}
