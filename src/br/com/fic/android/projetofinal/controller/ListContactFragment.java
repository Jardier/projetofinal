package br.com.fic.android.projetofinal.controller;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import br.com.fic.android.projetofinal.R;

public class ListContactFragment extends ListFragment {

	OnContatoSelectedListener callBack;

	public interface OnContatoSelectedListener {
		public void onContatoSelected(int position);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// We need to use a different list item layout for devices older than
		// Honeycomb
		int layout = Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ? android.R.layout.simple_list_item_activated_1 : android.R.layout.simple_list_item_1;

		// Create an array adapter for the list view, using the Ipsum headlines
		// array
		setListAdapter(new ArrayAdapter<String>(getActivity(), layout, getNomesContatos()));
	}

	@Override
	public void onStart() {
		super.onStart();

		// When in two-pane layout, set the listview to highlight the selected
		// list item
		// (We do this during onStart because at the point the listview is
		// available.)
		if (getFragmentManager().findFragmentById(R.id.line_fragment) != null) {
			getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		}
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			callBack = (OnContatoSelectedListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " implementação do OnContatoSelectedListener ");
		}
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		// Notify the parent activity of selected item
		callBack.onContatoSelected(position);

		// Set the item as checked to be highlighted when in two-pane layout
		getListView().setItemChecked(position, true);
	}

	private List<String> getNomesContatos() {
		List<String> lista = new ArrayList<String>();
		Uri CONTENT_URI = ContactsContract.Contacts.CONTENT_URI;
		String DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME;

		ContentResolver contentResolver = getActivity().getContentResolver();
		Cursor cursor = contentResolver.query(CONTENT_URI, null, null, null, null);

		if (cursor.getCount() > 0) {
			while (cursor.moveToNext()) {
				String name = cursor.getString(cursor.getColumnIndex(DISPLAY_NAME));
				lista.add(name);
			}
		}
		return lista;

	}

}
